import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_flickr/flickrPhoto.dart';
import 'package:flutter_flickr/flickrResult.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FlickrMainPage()
    );
  }
}

class FlickrMainPage extends StatefulWidget {
  FlickrMainPage({Key key}) : super(key: key);

  @override
  _FlickrMainPageState createState() => _FlickrMainPageState();
}

class _FlickrMainPageState extends State<FlickrMainPage> {

  final String flickrKey = "YOUR_APP_KEY_HERE";
  FlickrResult flickrData;
  int resultsCount = 0;

  @override
  void initState() {
    super.initState();

    loadFlickrData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('App Bar Text')
      ),
      body: new ListView.builder(
        itemCount: resultsCount,
        itemBuilder: (BuildContext context, int position) {
          return getRow(position);
        }),
      );
  }

  Widget getRow(int i) {

    FlickrPhoto photo = flickrData.photos.photo[i];
    String farm = photo.farm.toString();
    String secret = photo.secret;
    String server = photo.server;

    String photoUrl = "https://farm$farm.staticflickr.com/$server/${photo.id}_$secret.jpg";

    return Container(
      padding: new EdgeInsets.all(10.0),
      child: Image.network(photoUrl, height: 240.0, fit: BoxFit.cover),
    );

  }

  loadFlickrData() async {

    String requestUrl = "https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=" +
        flickrKey +
        "&format=json&nojsoncallback=1";

    http.Response response = await http.get(requestUrl);

    setState(() {
      Map userMap = jsonDecode(response.body);
      flickrData = FlickrResult.fromJson(userMap);
      resultsCount = flickrData.photos.photo.length;
    });

  }
}